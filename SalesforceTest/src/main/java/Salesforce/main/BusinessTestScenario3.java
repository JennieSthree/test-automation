package Salesforce.main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class BusinessTestScenario3 {

    public void BusinessTestScenario3 () throws InterruptedException {

        browserDrivers chrome = new browserDrivers();
        WebDriver driver = chrome.startChrome();

        WebElement username = driver.findElement(By.name("username"));
        username.sendKeys("jcon@sthree.com.qa");

        WebElement password = driver.findElement(By.name("pw"));
        password.sendKeys("Samt1234#");

        WebElement login = driver.findElement(By.name("Login"));
        login.submit();

        TimeUnit.SECONDS.sleep(3);

        WebElement AccountTab = driver.findElement(By.id("Account_Tab"));
        AccountTab.click();

        TimeUnit.SECONDS.sleep(3);

        WebElement newBusinessButton = driver.findElement(By.name("new"));
        newBusinessButton.click();

        Select businessType = new Select(driver.findElement(By.id("p3")));
        String businessTypeDrop = businessType.getFirstSelectedOption().getText();

        WebElement clickContinue = driver.findElement(By.name("save"));
        clickContinue.submit();

        WebElement cancel = driver.findElement(By.name("cancel"));
        cancel.click();

        WebElement businessPage = driver.findElement(By.id("fcf"));

        TimeUnit.SECONDS.sleep(3);
        if(businessPage.isDisplayed())
        {
            System.out.println("Test has Passed B3");

        }
        else {
            System.out.println("something is wrong with this test b3");
        }




        driver.quit();



    }

}
