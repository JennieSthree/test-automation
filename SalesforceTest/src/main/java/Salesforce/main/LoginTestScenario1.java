package Salesforce.main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class LoginTestScenario1 {

    public void LoginTest1() throws InterruptedException {

       browserDrivers chrome = new browserDrivers();
       WebDriver driver = chrome.startChrome();

        WebElement login = driver.findElement(By.name("Login"));
        login.submit();

        WebElement loginText = driver.findElement(By.name("username"));

        TimeUnit.SECONDS.sleep(3);

        if ((loginText.isDisplayed())) {
            System.out.println("You were unable to login s1 ");
        } else
            System.out.println("something is wrong with this test s1");
        driver.quit();
    }

}

