package Salesforce.main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class LoginTestScenario6 {

    // Login with a Correct username and Password

    public void LoginTestScenario6 () throws InterruptedException {

        browserDrivers chrome = new browserDrivers();
        WebDriver driver = chrome.startChrome();

        WebElement username = driver.findElement(By.name("username"));
        username.sendKeys("jcon@sthree.com.qa");

        WebElement password = driver.findElement(By.name("pw"));
        password.sendKeys("Samt1234#");

        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("username")));

        WebElement login = driver.findElement(By.name("Login"));
        login.submit();

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        WebElement contactTab = driver.findElement(By.id("Contact_Tab"));

//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        TimeUnit.SECONDS.sleep(3);

        if ((contactTab.isDisplayed()))
        {
            System.out.println("You were logged in s6");

            WebElement usernav = driver.findElement(By.id("userNavButton"));
            usernav.click();

            WebElement logout = driver.findElement(By.xpath("//*[@id=\"userNav-menuItems\"]/a[4]"));
            logout.click();

            TimeUnit.SECONDS.sleep(3);

            WebElement verifyLogout = driver.findElement(By.id("forgot_password_link"));

            if((verifyLogout.isDisplayed()))
            {
                System.out.println("You were logged out s6");
            }
            else
                System.out.println("Something went wrong s6");


        }
        else {
            System.out.println("something is wrong with this test s6");
            driver.quit();
        }



    }
}
