package Salesforce.main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class BusinessTestScenario6 {

    public void BusinessTestScenario6 () throws InterruptedException {

        browserDrivers chrome = new browserDrivers();
        WebDriver driver = chrome.startChrome();

        WebElement username = driver.findElement(By.name("username"));
        username.sendKeys("jcon@sthree.com.qa");

        WebElement password = driver.findElement(By.name("pw"));
        password.sendKeys("Samt1234#");

        WebElement login = driver.findElement(By.name("Login"));
        login.submit();

        TimeUnit.SECONDS.sleep(3);

        WebElement AccountTab = driver.findElement(By.id("Account_Tab"));
        AccountTab.click();

        TimeUnit.SECONDS.sleep(3);

        WebElement newBusinessButton = driver.findElement(By.name("new"));
        newBusinessButton.click();

        Select businessType = new Select(driver.findElement(By.id("p3")));
        String businessTypeDrop = businessType.getFirstSelectedOption().getText();

        WebElement clickContinue = driver.findElement(By.name("save"));
        clickContinue.submit();

        TimeUnit.SECONDS.sleep(2);

        WebElement clickContinue1 = driver.findElement(By.name("save"));
        clickContinue1.click();

        WebElement errorMsg = driver.findElement(By.xpath("/html/body/div/div[2]/table/tbody/tr/td[2]/form/div/div[2]/div[4]/table/tbody/tr[1]/td[2]/div/input"));

        if (errorMsg.isDisplayed()){
            System.out.println("Error message "+errorMsg);
        }
        else
        {
            System.out.println("Something went wrong");
        }



//        WebElement clickContinue1 = driver.findElement(By.name("save"));
//        clickContinue1.click();
//
//        WebElement errorMsg = driver.findElement(By.id("errorDiv_ep"));
//
//        if (errorMsg.isDisplayed()){
//
//            WebElement businessError = driver.findElement(By.xpath("/html/body/div[1]/div[2]/table/tbody/tr/td[2]/form/div/div[2]/div[4]/table/tbody/tr[1]/td[2]/div/input"));
//
//
//            Class<? extends WebElement> verifyBusinessError = businessError.getClass();
//
//            System.out.println(verifyBusinessError +" Is there");
//
//
//
//        }
//
//        else {
//
//        }







    }

    }
