package Salesforce.main;

import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ISelect;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import static java.util.concurrent.ThreadLocalRandom.*;

public class businessCreation {

//    Faker faker = new Faker();
//
//    String businessName = faker.company().name();
//    String businessPhone = faker.phoneNumber().phoneNumber();

    //the following code opens the business page

    public void openBusinessPage (WebDriver driver) throws InterruptedException {
        WebElement businessPage = driver.findElement(By.id("Account_Tab"));
        if (businessPage.isDisplayed()) {
           // System.out.println("Create business available");

            businessPage.click();
//            Thread.sleep(3000);
        } else {
            driver.quit();
        }

    }

    // the following code clicks the new button on the business page
    public void newBusiness (WebDriver driver)
    {
        WebElement newBusinessButton = driver.findElement(By.name("new"));

        if (newBusinessButton.isDisplayed()){
            System.out.println("Button available");
            newBusinessButton.click();
        }
        else
        {
            driver.quit();
        }

    }

    public void selectingBusinessType (WebDriver driver) throws InterruptedException {
        Thread.sleep(2000);// change to if webpage is loaded

        Select businessType = new Select(driver.findElement(By.id("p3")));
        String businessTypeDrop = businessType.getFirstSelectedOption().getText();


        if(businessTypeDrop.matches("Business"))
        {
            System.out.println(businessTypeDrop);
        }
        else
        {
            driver.quit();
        }

    }

    public void clickingContinue (WebDriver driver)
    {
        WebElement clickContinue = driver.findElement(By.name("save"));

        if (clickContinue.isEnabled())
        {
            clickContinue.submit();
        }

        else
        {
            driver.quit();
        }

    }

    public void newBusinessData (WebDriver driver) throws InterruptedException {

        Faker faker = new Faker();
//        Thread.sleep(2000);
        WebElement biznm = driver.findElement(By.id("acc2"));
        WebElement bizphn   = driver.findElement(By.id("acc10"));


        if (biznm.isDisplayed())
        {
            String businessName = faker.company().name();
            String businessPhone = faker.phoneNumber().phoneNumber();

            biznm.sendKeys(businessName);
            bizphn.sendKeys(businessPhone);

            //randomly selecting from a dropdown
           // new WebDriverWait.FIVE_HUNDRED_MILLIS;
            Select drpIndustrydown = new Select(driver.findElement(By.id("acc7")));
            List<WebElement> dd = drpIndustrydown.getOptions();
            int size = dd.size();
            int rndnum = ThreadLocalRandom.current().nextInt(0,size);
            drpIndustrydown.selectByIndex(rndnum);

            //Selecting a brand at random
            Thread.sleep(2000);
            Select drpBranddown = new Select(driver.findElement(By.id("00N2400000EoxRy")));
            List<WebElement> dbd = drpBranddown.getOptions();
            int size4 = dbd.size();
            int rndnum4 = ThreadLocalRandom.current().nextInt(1,size4);
            drpBranddown.selectByIndex(rndnum4);



            //Selecting Billing country
            Select drpCountrydown = new Select(driver.findElement(By.id("acc17country")));
            List<WebElement> dcd = drpCountrydown.getOptions();
            int size1 = dcd.size();
            int rndnum1 = ThreadLocalRandom.current().nextInt(0,size1);
            drpCountrydown.selectByIndex(rndnum1);

            WebElement bizstate = driver.findElement(By.id("acc17state"));
            if (bizstate.isEnabled())
            {
                Select drpStatedown = new Select(driver.findElement(By.id("acc17state")));
                List<WebElement> dsd = drpStatedown.getOptions();
                int size2 = dsd.size();
                int rndnum2 = ThreadLocalRandom.current().nextInt(0,size2);
                drpStatedown.selectByIndex(rndnum2);
            }


            WebElement bizstreet = driver.findElement(By.id("acc17street"));
            String busStreet = faker.address().streetAddress();
            bizstreet.sendKeys(busStreet);

            WebElement bizcity = driver.findElement(By.id("acc17city"));
            String busCity = faker.address().city();
            bizcity.sendKeys(busCity);

            WebElement bizzip = driver.findElement(By.id("acc17zip"));
            String busZip = faker.address().zipCode();
            bizzip.sendKeys(busZip);




        }
        else
        {
            driver.quit();
        }

        WebElement clickContinue = driver.findElement(By.name("save"));

        if (clickContinue.isEnabled())
        {
            clickContinue.click();
        }

        else
        {
            driver.quit();
        }



    }




}
