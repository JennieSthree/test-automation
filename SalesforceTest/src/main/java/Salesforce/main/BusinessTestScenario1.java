package Salesforce.main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BusinessTestScenario1 {

    // Account tab is available

    public void BusinessTestScenario1 () throws InterruptedException {

        browserDrivers chrome = new browserDrivers();
        WebDriver driver = chrome.startChrome();

        WebElement username = driver.findElement(By.name("username"));
        username.sendKeys("jcon@sthree.com.qa");

        WebElement password = driver.findElement(By.name("pw"));
        password.sendKeys("Samt1234#");

        WebElement login = driver.findElement(By.name("Login"));
        login.submit();

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        WebElement AccountTab = driver.findElement(By.id("Account_Tab"));

//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        TimeUnit.SECONDS.sleep(3);

        if ((AccountTab.isDisplayed()))
        {
            System.out.println("You were logged in b1");
        }
        else
            System.out.println("something is wrong with this test b1");
        driver.quit();



    }
}
