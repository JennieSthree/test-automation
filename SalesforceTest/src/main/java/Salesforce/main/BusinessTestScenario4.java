package Salesforce.main;

import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.*;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class BusinessTestScenario4 {

    // user creating a business and a Manager

    public void BusinessTestScenario4 () throws InterruptedException {

        // Starting Driver

        browserDrivers chrome = new browserDrivers();
        WebDriver driver = chrome.startChrome();

        //Sends Username

        WebElement username = driver.findElement(By.name("username"));
        username.sendKeys("jcon@sthree.com.qa");

        //Sends Password

        WebElement password = driver.findElement(By.name("pw"));
        password.sendKeys("Samt1234");

        //Clicks Login

        WebElement login = driver.findElement(By.name("Login"));
        login.submit();

        //Waits for Login to complete

        WebDriverWait wait1 = new WebDriverWait(driver, 10);
        wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("Account_Tab")));

        //clicks on the account Tab

        WebElement AccountTab = driver.findElement(By.id("Account_Tab"));
        AccountTab.click();

        //Waits for the page to load and clicks new

        WebDriverWait wait2 = new WebDriverWait(driver, 10);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.name("new"))).click();

        //selects business from the dropdown

        Select businessType = new Select(driver.findElement(By.id("p3")));
        businessType.getFirstSelectedOption().getText();

        //clicks save

        driver.findElement(By.name("save")).submit();

        //Finds the business name and phone number and then use faker to add in the data

        Faker faker = new Faker();
//        Thread.sleep(2000);
        driver.findElement(By.id("acc2")).sendKeys(faker.company().name());
        driver.findElement(By.id("acc10")).sendKeys(faker.phoneNumber().phoneNumber());

        //randomly selecting from a dropdown

        //Industy list

        Select drpIndustrydown = new Select(driver.findElement(By.id("acc7")));
        List<WebElement> dd = drpIndustrydown.getOptions();
        int size = dd.size();
        int rndnum = ThreadLocalRandom.current().nextInt(1, size);
        drpIndustrydown.selectByIndex(rndnum);

        //Brand List

        Select drpBranddown = new Select(driver.findElement(By.id("00N2400000EoxRy")));
        List<WebElement> dbd = drpBranddown.getOptions();
        int size4 = dbd.size();
        int rndnum4 = ThreadLocalRandom.current().nextInt(1, size4);
        drpBranddown.selectByIndex(rndnum4);

        //Country List

        Select drpCountrydown = new Select(driver.findElement(By.id("acc17country")));
        List<WebElement> dcd = drpCountrydown.getOptions();
        int size1 = dcd.size();
        int rndnum1 = ThreadLocalRandom.current().nextInt(0, size1);
        drpCountrydown.selectByIndex(rndnum1);

        WebElement bizstate = driver.findElement(By.id("acc17state"));
        if (bizstate.isEnabled()) {
            Select drpStatedown = new Select(driver.findElement(By.id("acc17state")));
            List<WebElement> dsd = drpStatedown.getOptions();
            int size2 = dsd.size();
            int rndnum2 = ThreadLocalRandom.current().nextInt(1, size2);
            drpStatedown.selectByIndex(rndnum2);
        }


        driver.findElement(By.id("acc17street")).sendKeys(faker.address().streetAddress());
        driver.findElement(By.id("acc17city")).sendKeys(faker.address().city());
        driver.findElement(By.id("acc17zip")).sendKeys(faker.address().zipCode());


        driver.findElement(By.name("save")).click();


        WebDriverWait wait3 = new WebDriverWait(driver, 10);
        wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Done']"))).click();

//        WebElement closeCodes = driver.findElement(By.xpath("//button[text()='Done']"));
//        closeCodes.click();

        for (int i = 1; i <= 2; i++) {


            WebDriverWait wait4 = new WebDriverWait(driver, 10);
            wait4.until(ExpectedConditions.visibilityOfElementLocated(By.name("newContact"))).click();

//            WebElement contactVerify = driver.findElement(By.name("newContact"));
//
//            contactVerify.isDisplayed();
//            contactVerify.isEnabled();
//            contactVerify.click();


            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("p3")));


            Select contactType = new Select(driver.findElement(By.id("p3")));
            contactType.getFirstSelectedOption().getText();

            driver.findElement(By.name("save")).submit();

            WebDriverWait wait5 = new WebDriverWait(driver, 15);
            wait5.until(ExpectedConditions.visibilityOfElementLocated(By.id("name_salutationcon2")));


            Select salutationdrpdown = new Select(driver.findElement(By.id("name_salutationcon2")));
            List<WebElement> sdd = salutationdrpdown.getOptions();
            int size2 = sdd.size();
            int rndnum2 = ThreadLocalRandom.current().nextInt(0, size2);
            salutationdrpdown.selectByIndex(rndnum2);

            driver.findElement(By.id("name_firstcon2")).sendKeys(faker.name().firstName());
            driver.findElement(By.id("name_lastcon2")).sendKeys(faker.name().lastName());
            driver.findElement(By.id("con15")).sendKeys(faker.internet().emailAddress());


            Select mailshotdrpdown = new Select(driver.findElement(By.id("00N2400000EoxXR")));
            List<WebElement> mdd = mailshotdrpdown.getOptions();
            int size3 = mdd.size();
            int rndnum3 = ThreadLocalRandom.current().nextInt(1, size3);
            mailshotdrpdown.selectByIndex(rndnum3);

            driver.findElement(By.name("save")).click();

            WebDriverWait wait6 = new WebDriverWait(driver, 10);
            wait6.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Done']"))).click();


            WebDriverWait wait7 = new WebDriverWait(driver, 10);
            wait7.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/table/tbody/tr/td[2]/div[3]/div[2]/div[2]/table/tbody/tr[6]/td[2]/a")));
            WebElement managerBusiness = driver.findElement(By.xpath("/html/body/div[1]/div[2]/table/tbody/tr/td[2]/div[3]/div[2]/div[2]/table/tbody/tr[6]/td[2]/a"));

            if ((managerBusiness.isDisplayed())){
                managerBusiness.click();

                WebElement codesClose2 = driver.findElement(By.xpath("//*[@id='codesModal']/div/div/div[3]/button"));

                if ((codesClose2.isDisplayed())){
                    WebDriverWait wait8 = new WebDriverWait(driver, 30);
                    wait8.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='codesModal']/div/div/div[3]/button")));
                    codesClose2.click();
                }

                else
                {

                }

            }
            else
            {

            }

        }

            WebElement editVerify = driver.findElement(By.name("edit"));


            if ((editVerify.isDisplayed())) {
                System.out.println("Test has Passed B4");

            } else {
                System.out.println("something is wrong with this test b4");
            }

            driver.quit();


        }
    }





