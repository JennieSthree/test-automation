package Salesforce.main;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class browserDrivers {


    public WebDriver startChrome() {


        String loggedUser = System.getProperty("user.name");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\"+loggedUser+"\\ChromeDriver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://test.salesforce.com");
        return driver;

    }
}
