package Salesforce.main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class LoginTestScenario7 {

    // Login with a Correct username and old Password

    public void LoginTestScenario7 () throws InterruptedException {

        browserDrivers chrome = new browserDrivers();
        WebDriver driver = chrome.startChrome();

        WebElement username = driver.findElement(By.name("username"));
        username.sendKeys("jcon@sthree.com.qa");

        WebElement password = driver.findElement(By.name("pw"));
        password.sendKeys("Samt1234*");

//        WebDriverWait wait = new WebDriverWait(driver,10);
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("username")));

        WebElement login = driver.findElement(By.name("Login"));
        login.submit();

        WebElement loginText = driver.findElement(By.name("username"));

//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        TimeUnit.SECONDS.sleep(3);

        if ((loginText.isDisplayed()))
        {
            System.out.println("You were unable to login s7");
        }
        else
            System.out.println("something is wrong with this test s7");
        driver.quit();




    }



}
