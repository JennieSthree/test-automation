// This code has issues around the codes as it will select all codes and we cant limit it to remove some of the
// unused brands (TR/S3/HF) would need to look at this for moving forward.


package Salesforce.main;

import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class BusinessTestScenario5 {

    //Multi brand user creating a business

    public void BusinessTestScenario5 () throws InterruptedException {

        browserDrivers chrome = new browserDrivers();
        WebDriver driver = chrome.startChrome();;

        WebElement username = driver.findElement(By.name("username"));
        username.sendKeys("jcon@sthree.com.qa");

        WebElement password = driver.findElement(By.name("pw"));
        password.sendKeys("Samt1234#");

        WebElement login = driver.findElement(By.name("Login"));
        login.submit();

        TimeUnit.SECONDS.sleep(3);

        WebElement AccountTab = driver.findElement(By.id("Account_Tab"));
        AccountTab.click();

        TimeUnit.SECONDS.sleep(3);

        WebElement newBusinessButton = driver.findElement(By.name("new"));
        newBusinessButton.click();

        Select businessType = new Select(driver.findElement(By.id("p3")));
        businessType.getFirstSelectedOption().getText();

        WebElement clickContinue = driver.findElement(By.name("save"));
        clickContinue.submit();

        Faker faker = new Faker();
//        Thread.sleep(2000);
        WebElement biznm = driver.findElement(By.id("acc2"));
        WebElement bizphn   = driver.findElement(By.id("acc10"));

        String businessName = faker.company().name();
        String businessPhone = faker.phoneNumber().phoneNumber();

        biznm.sendKeys(businessName);
        bizphn.sendKeys(businessPhone);

        //randomly selecting from a dropdown

        Select drpIndustrydown = new Select(driver.findElement(By.id("acc7")));
        List<WebElement> dd = drpIndustrydown.getOptions();
        int size = dd.size();
        int rndnum = ThreadLocalRandom.current().nextInt(0,size);
        drpIndustrydown.selectByIndex(rndnum);

        Select drpBranddown = new Select(driver.findElement(By.id("00N2400000EoxRy")));

//        Select list = new Select(driver.findElement(By.id("00N2400000EoxRy")));
        List<WebElement> dbd = drpBranddown.getOptions();

//        int size4 = dbd.size();
// //       int rndnum4 = ThreadLocalRandom.current().nextInt(1, size4);
//
//
//
//        for (int i =0; i < dbd.size(); i++){
//            int rndnum4 = ThreadLocalRandom.current().nextInt(1, size4);
//
//            String value = dbd.get(i).getText();
//
//
//            System.out.println(value);
//
//            System.out.println(value1);
//            System.out.println(rndnum4);
//
//
//        }

        int size4 = dbd.size();
        int rndnum4 = ThreadLocalRandom.current().nextInt(1, size4);
        drpBranddown.selectByIndex(rndnum4);


        Select drpCountrydown = new Select(driver.findElement(By.id("acc17country")));
        List<WebElement> dcd = drpCountrydown.getOptions();
        int size1 = dcd.size();
        int rndnum1 = ThreadLocalRandom.current().nextInt(0,size1);
        drpCountrydown.selectByIndex(rndnum1);

        WebElement bizstate = driver.findElement(By.id("acc17state"));
        if (bizstate.isEnabled())
        {
            Select drpStatedown = new Select(driver.findElement(By.id("acc17state")));
            List<WebElement> dsd = drpStatedown.getOptions();
            int size2 = dsd.size();
            int rndnum2 = ThreadLocalRandom.current().nextInt(1,size2);
            drpStatedown.selectByIndex(rndnum2);
        }


        WebElement bizstreet = driver.findElement(By.id("acc17street"));
        String busStreet = faker.address().streetAddress();
        bizstreet.sendKeys(busStreet);

        WebElement bizcity = driver.findElement(By.id("acc17city"));
        String busCity = faker.address().city();
        bizcity.sendKeys(busCity);

        WebElement bizzip = driver.findElement(By.id("acc17zip"));
        String busZip = faker.address().zipCode();
        bizzip.sendKeys(busZip);


        WebElement clickContinue1 = driver.findElement(By.name("save"));
        clickContinue1.click();

        TimeUnit.SECONDS.sleep(3);

        WebElement closeCodes = driver.findElement(By.xpath("//button[text()='Done']"));
        closeCodes.click();

        WebElement editVerify = driver.findElement(By.name("edit"));

        TimeUnit.SECONDS.sleep(3);
        if((editVerify.isDisplayed()))
        {
            System.out.println("Test has Passed B4");

        }
        else {
            System.out.println("something is wrong with this test b3");
        }
        driver.quit();



    }
}
